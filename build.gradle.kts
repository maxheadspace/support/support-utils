import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jetbrains.dokka.gradle.DokkaTask

plugins {
    java
    kotlin("jvm") version "1.3.50"
    id("com.eden.orchidPlugin") version("0.17.7")
    id("org.jetbrains.dokka") version("0.10.0")
}

group = "org.maxheadspace.support"

version = "1.0-SNAPSHOT"
repositories {
    mavenCentral()
    jcenter()
}


dependencies {

    implementation(kotlin("stdlib-jdk8"))
    compileOnly("org.slf4j:slf4j-api:1.7.29")

    // Orchid Site Generator
    val orchidVersion = "0.17.7"
    orchidRuntime("io.github.javaeden.orchid:OrchidDocs:$orchidVersion")
    orchidRuntime("io.github.javaeden.orchid:OrchidKotlindoc:$orchidVersion")
    orchidRuntime("io.github.javaeden.orchid:OrchidPluginDocs:$orchidVersion")
    orchidRuntime("io.github.javaeden.orchid:OrchidGitlab:$orchidVersion")
    orchidRuntime("io.github.javaeden.orchid:OrchidSearch:$orchidVersion")

    // Unit Testing
    val junitVersion = "5.5.2"
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$junitVersion")
}

tasks.test {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}
tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

orchid {
    theme = "Editorial"
    baseUrl = "https://gitlab.com/maxheadspace/support/support-utils/wikis/"
    version = "${project.version}"
}


