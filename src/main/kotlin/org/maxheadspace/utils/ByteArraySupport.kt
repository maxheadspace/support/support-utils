package org.maxheadspace.utils

import java.util.*
import java.util.Arrays.hashCode as deepHashOf

@Suppress("SpellCheckingInspection")
private val hexChars = "0123456789ABCDEF".toCharArray()

private fun StringBuilder.appendHexChar(position: Int, lowerCase: Boolean) {
    hexChars[position].let { x ->
        if (lowerCase) {
            append(x.toLowerCase())
        } else append(x)
    }
}

/**
 * Converts a byte array to hex value string. Suited for debugging purposes etc.
 *
 * @receiver Array of bytes to convert to hex.
 * @return A hex string.
 * @param lowerCase Converts the chars to lower case
 */
fun ByteArray.toHex(lowerCase: Boolean = false): String = buildString(capacity = size * 2) {
    this@toHex.asSequence().map { it.toInt() and 0xff }.forEach { byte ->
        appendHexChar(byte shr 4, lowerCase)
        appendHexChar(byte and 0x0f, lowerCase)
    }
}

private typealias Base64EncoderConfigBlock = Base64.Encoder.() -> Base64.Encoder
private typealias Base64DecoderConfigBlock = Base64.Decoder.() -> Base64.Decoder

/**
 * Converts an byte array to base 64 representation which can be passed around as text
 *
 * @receiver The bytes which should be used to represent as a base64 string.
 * @return The base 64 encoded byte array.
 * @param configureEncoder A code block which can be used configure the default [Base64.Decoder] further.
 */
fun ByteArray.toBase64String(configureEncoder: Base64EncoderConfigBlock? = null): String = Base64.getEncoder()
    .apply { if (configureEncoder != null) configureEncoder() }
    .encodeToString(this)

/**
 * Converts a base64 encoded string to an [ByteArray]
 *
 * @receiver The base64 encoded byte array (as text).
 * @param configureDecoder A code block which can be used to configure the [Base64.Decoder] further.
 * @return The decoded [ByteArray]
 */
fun String.toBase64Bytes(configureDecoder: Base64DecoderConfigBlock? = null): ByteArray = Base64
    .getDecoder()
    .apply { if (configureDecoder != null) configureDecoder() }
    .decode(this)

fun ByteArray.hash(): Int = deepHashOf(this)
