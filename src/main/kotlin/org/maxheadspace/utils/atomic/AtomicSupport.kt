package org.maxheadspace.utils.atomic

import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.AtomicReference
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * Produces an [AtomicReference] from a value.
 */
fun <V> atomicOf(value: V): AtomicReference<V> = AtomicReference(value)

/**
 * Produces an [AtomicBoolean] of a value.
 */
fun atomicOf(boolean: Boolean): AtomicBoolean = AtomicBoolean(boolean)

/**
 * Produces an [AtomicInteger] of [Int] value.
 */
fun atomicOf(int: Int): AtomicInteger = AtomicInteger(int)

/**
 * Test to make sure the atomic reference is actually null.
 */
fun <V> AtomicReference<V>.isNull(): Boolean = get() == null

/**
 * Forces the value of an [AtomicReference] to be `null`
 */
fun <V> AtomicReference<V?>.unset(): Unit = set(null)

var <V> AtomicReference<V>.value: V
    get() = get()
    set(value) = set(value)


private class AtomicProperty<V>(private val ref: AtomicReference<V>) : ReadWriteProperty<Any?, V> {

    override fun getValue(thisRef: Any?, property: KProperty<*>): V = ref.get()

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: V) {
        ref.set(value)
    }
}

/**
 * Converts any [AtomicReference] to object variable
 */
fun <V> AtomicReference<V>.property(): ReadWriteProperty<Any?, V> = AtomicProperty(this)


var AtomicBoolean.boolean: Boolean
    get() = get()
    set(value) = set(value)