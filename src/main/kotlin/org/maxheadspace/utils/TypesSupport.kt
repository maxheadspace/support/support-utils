package org.maxheadspace.utils

/**
 * Evaluates [functionBlock] to a result.
 *
 * Unlike the [run] function this function does not require a receiver, so it can be safely called
 * from inside a constructor. For example to perform some complex
 * computation, before setting a field.
 *
 * @param functionBlock The block of an anonymous function.
 * @return [T] Result of the function block
 */
fun <T> eval(functionBlock: () -> T): T = functionBlock()

/**
 * Function to map a `true` of `false` value to [truth] or [notTrue] value.
 *
 * @param truth The value represents a truth
 * @param notTrue The value represents an untruth.
 */
fun <T> Boolean.truth(truth: T, notTrue: T): T {
    return if (this) truth else notTrue
}

/**
 * A handy function to capture the actual type, as the compiler sees it.
 */
inline fun <reified T> classOf(): Class<T> = T::class.java

/**
 * A helper function to ensure _nulls_ can be see as _typed_ `null` by the compiler.
 */
inline fun <reified T> nullOf(): T? = null

private fun nextHash(currentHash: Int, any: Any?): Int = (currentHash * 31) + (any?.hashCode() ?: 0)

/**
 * Computes a JVM based hash for a sequence of things.
 */
fun Sequence<*>.computeHash(): Int = fold(1, ::nextHash)

/**
 * Computes a JVM based hash for a list of things.
 */
fun List<*>.computeHash(): Int = fold(1, ::nextHash)
