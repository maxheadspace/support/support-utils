package org.maxheadspace.utils

private class TransposedMutableMap<K, V>(private val target: MutableMap<K, V>) : AbstractMutableMap<V, K>() {

    private class TransposedEntry<K, V>(
        private val target: MutableMap<K, V>,
        initialKey: K,
        initialValue: V
    ) : MutableMap.MutableEntry<V, K> {

        override var key: V = initialValue
            private set(update) {
                if (update != field) {
                    val entry = target.entries.find { it.value == field }
                    if (entry == null) {
                        target[value] == update
                    } else {
                        entry.setValue(update)
                    }
                    field = update
                }
            }

        override var value: K = initialKey
            private set(update) {
                if (update != field) {
                    target[update] = key
                    field = update
                }
            }

        override fun setValue(newValue: K): K {
            val oldValue = value
            value = newValue
            return oldValue
        }
    }

    override val entries: MutableSet<MutableMap.MutableEntry<V, K>>
        get() = target.entries.asSequence().map { (k, v) ->
            TransposedEntry(
                target,
                k,
                v
            )
        }.toMutableSet()

    override fun put(key: V, value: K): K? {
        val current = target.entries.find { (k, _) -> k == value }
        return when {
            current == null -> {
                target[value] = key
                null
            }
            current.key == value && current.value == key -> current.key
            else -> {
                val discarded = current.key
                target.remove(current.key)
                target[value] = key
                discarded
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false
        other as TransposedMutableMap<*, *>
        if (target != other.target) return false
        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + target.hashCode()
        return result
    }

}

/**
 * Transpose a mutable map to live (operations transposed map are propagated).
 *
 * @receiver The map you want to transpose.
 * @param K The key
 * @param V The value
 *
 * @return A transposed [MutableMap]`<V,K>`
 */
fun <K, V> MutableMap<K, V>.transposeLive(): MutableMap<V, K> = TransposedMutableMap(this)

fun <K, V> Map<K, V>.transpose(): Map<V, K> = entries.asSequence().map { (k, v) -> v to k }.toMap()

/**
 * Creates a unique list of elements from an existing list.
 */
fun <E> List<E>.toUniqueList(): List<E> = LinkedHashSet(this).toList()
